# Hardware

This project documents the setup of the Raspberry Pi cloud with all its components

## Shopping list
| Amount | Part | Price |
| ------ | ---- | ----- |
| 1 | [TP-Link TL-MR3020](https://www.amazon.de/dp/B00634PLTW) | 25 EUR |
| 1 | [TP-Link TL-SG108E](https://www.amazon.de/dp/B00JKB63D8) | 33 EUR |
| 1 | [Raspberry Pi 2B](https://www.amazon.de/dp/B00T2U7R7I/) | re-use existing |
| 1 | [3.5" touch screen display](https://www.amazon.de/dp/B01JRUH0CY) | re-use existing |
| 1 | [LUX Werkzeugkoffer AKW 130](https://www.obi.at/werkzeugkoffer/lux-werkzeugkoffer-akw-130/p/6678213) | 12 EUR |
| 4 | [SanDisk Ultra MicroSDHC 32GB](https://www.amazon.de/dp/B012VKWY7I/) | 40 EUR |
| 1 | [Anker PowerPort 6 60W](https://www.amazon.de/dp/B00PTLSH9G/) | 28 EUR |
| 4 | [Raspberry Pi 3B](https://www.amazon.de/gp/product/B01CD5VC92/) | 160 EUR |
| 1 | [5 pack Raspberry Pi aluminium cooler](https://www.amazon.de/dp/B01N6POSOR/) | 9 EUR |
| 5 | [CAT6 Netzwerkkabel 0.5m](https://www.amazon.de/gp/product/B007AT5586/) | 8 EUR |
| 1 | [ 6 pack USB cable](https://www.amazon.de/gp/product/B00ZGVMNRQ) | 9 EUR |
| 1 | [ Power strip euro plugs](https://www.amazon.de/gp/product/B00JEAS0L8/) | 12 EUR |
| 1 | [Decoration foil](https://www.obi.at/klebefolien-funktionsfolien/d-c-fix-klebefolie-schwarz-matt-45-cm-x-200-cm/p/2239242) | 6 EUR |
| 1 | Div. screws | 20 EUR |
| 1 | Cable organizer | 6 EUR |
||| **368 EUR** |

Picture off all parts:
![parts](img/parts.JPG)

## Initial setup
All the Raspberry Pis are running [Raspbian Stretch Lite](https://www.raspberrypi.org/downloads/raspbian/). The subfolder
'parts' describes the hardware setup in more detail. Overview:

**admin01:**
This node is used to run the docker containers for monitoring and is equipped with a display

**app01-03:**
Those nodes are used as application nodes and will host the docker containers.

**master01:**
This node is the docker swarm manager node which manages the whole cluster.

**router:**
The WiFi router used to connect via WiFi to the whole cluster from external.

**suitcase:**
This describes the setup of the suitcase which hosts all components.

**switch:**
Description of the setup of the network switch which connects all Raspberry Pis.

## Infrastructure
<img src="img/rpi-cloud.png"/>
