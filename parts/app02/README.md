## General

This Raspberry Pi 3 is used as application node which will run the Docker containers

## Configuration

OS: Raspbian Stretch Lite

### Power settings

Power setting changes can be done with Ansible. For more details see
[Ansible Playbook for the Raspberry Pi cluster](https://gitlab.com/rpicloud/configuration)
in the role 'wireless'

***Manual installation***
- Prepare SD card with Raspbian Stretch Lite
  - sudo dd bs=4M if=2019-04-08-raspbian-stretch-lite.img of=/dev/mmcblk0 conv=fsync
- Disable WiFi and Bluetooth: Open the file /boot/config.txt and add the following lines at the end of the file:
```shell
# disable internal wifi and bluethooth
dtoverlay=pi3-disable-wifi,pi3-disable-bt
```

## Initial Setup

```shell
sudo raspi-config
```

- Network options
  - Hostname: app02
- Advanced options
  - Expand file system
  - Memory split: 16
