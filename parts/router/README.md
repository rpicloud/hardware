## General

The router provider WLAN access to the whole infrastructure and is also connected to the Internet router

## Custom OpenWRT build
Because of the limited space available on the router and the need of the installation of the ext4 driver and USB support, a custom build is necessary. Without a custom build it is not possible to install USB support including the ext4 driver.

Description of the [OpenWRT Image Generator](https://wiki.openwrt.org/doc/howto/obtain.firmware.generate).

### Steps to create the custom Image
Setup a VM with CentOS 7

Install needed packages
```
yum install subversion git gawk gettext ncurses-devel zlib-devel openssl-devel libxslt wget bzip2
yum group install "Development Tools"
```

```
cd ~
mkdir openwrt
cd openwrt
wget https://downloads.openwrt.org/chaos_calmer/15.05.1/ar71xx/generic/OpenWrt-ImageBuilder-15.05.1-ar71xx-generic.Linux-x86_64.tar.bz2
tar -xvjf OpenWrt-ImageBuilder-15.05.1-ar71xx-generic.Linux-x86_64.tar.bz2
cd OpenWrt-ImageBuilder-15.05.1-ar71xx-generic.Linux-x86_64
```

Start the build (we have to remove some packages which are not needed to get some free space to install luci)
```
make clean
make image PROFILE=TLMR3020 PACKAGES="kmod-usb-storage kmod-fs-ext4 block-mount luci luci-app-firewall luci-base luci-lib-ip luci-lib-nixio luci-mod-admin-full luci-proto-ipv6 luci-theme-bootstrap -kmod-ppp -kmod-pppoe -kmod-pppox -ppp -ppp-mod-pppoe"
```

Get the image from bin/ar71xx/openwrt-15.05.1-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin

## Change firmware over SSH without LuCi
```
wget -O /tmp/openwrt-15.05.1-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin http://downloads.openwrt.org/chaos_calmer/15.05.1/ar71xx/generic/openwrt-15.05.1-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin
mtd -r write /tmp/openwrt-15.05.1-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin firmware
```

## Configuration

The router has the custom built [OpenWRT 15.05.1](openwrt-15.05.1-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin) installed (https://wiki.openwrt.org/toh/tp-link/tl-mr3020)
The original firmware can be downloaded [here](TL-MR3020_V1_150921.zip).

### Setup AP+STA

This will configure the OpenWRT router to access the internet over wifi and provide it's own wifi to connect to the cluster:

First, enable STA mode, add two lines to the end of /etc/config/network.

```shell
config interface wwan
	option proto 'dhcp'
```
Then change /etc/config/wireless
```shell
config wifi-device 'radio0'
        option type 'mac80211'
        option hwmode '11g'
        option path 'platform/ar933x_wmac'
        option htmode 'HT20'
        option txpower '15'
        option channel '6'
        option country 'AT'

config wifi-iface
        option device 'radio0'
        option mode 'ap'
        option ssid 'OpenWrt'
        option encryption 'psk2'
        option key '<PW>'
        option network 'lan'

config wifi-iface
        option device 'radio0'
        option network 'wwan'
        option mode 'sta'
        option ssid '<SSID of Internet router>'
        option encryption 'psk2'
        option key '<PW>'
```
The internet router has taken 192.168.0.1 as its IP address, you have to change the setting in /etc/config/network, set openwrt ip to other, so it will not conflict with your house router in STA mode. The ip in example is 172.16.0.1.
```shell
config interface 'lan'
        option ifname 'eth0'
        option force_link '1'
        option type 'bridge'
        option proto 'static'
        option netmask '255.255.0.0'
        option ip6assign '60'
        option ipaddr '172.16.0.1'
```
After that you should be able to connect to internet and the local cluster network over the OpenWrt AP

## Static DHCP leases and hostnames

**DHCP leases (Setting: DNS and Hostnames):**
- switch 172.16.0.2
- admin01 172.16.0.11
- master01 172.16.0.21
- app01 172.16.0.31
- app02 172.16.0.32
- app03 172.16.0.33

**Hostnames (Setting: Hostnames):**
- switch 172.16.0.2
- admin01 172.16.0.11
- master01 172.16.0.21
- master 172.16.0.21 (load balancing url for all master nodes)
- app01 172.16.0.31
- app02 172.16.0.32
- app03 172.16.0.33

## Setup wildcard DNS for traefik
```
vi /etc/dnsmasq.conf
```

Add the following line at the end of the file and reboot the router:
```
# wildcard dns for traefik
address=/.rpicloud.io/172.16.0.21
```

Where the ip address points to the master01 which is running traefik as ingress
controller.

## NFS

The router has access to external storage and provides this as NFS to all Raspberry Pis
I use an old SSD with an external USB adapter which will be connected to the USB port of the router.

The SSD got the following partions:
* openwrt (1GB, EXT4): Extend the OpenWRT filesystem for additional software packages
* nfs (118GB, EXT4): Space for the NFS

### Setup
ATTENTION: The extended overlay partition has to be formatted as EXT4, FAT will not work!

Go to System - Mount points and add the 2 partitions (openwrt and ssd) and enable them.

The small partition will be used to extend the overlay filesystem and the bigger partition will be used for the NFS. With the following command we copy the old overlay directory to the external one on the SSD

```
tar -C /overlay -cvf - . | tar -C /mnt/openwrt -xf -
```

What does this do? Remember the firmware you flashed on to the router itself? Well that's of a fixed size, so it takes up a finite amount of space in the flash memory of the router and you cannot delete any 'unnecessary' packages to clear space, should you be short of space. BUT we need more space to add more functionality to our router.

Go to System - Mount Points and change the extended overlay filesystem to "Use as external overlay" and reboot the router to apply the changes. The mount points should look like this afterwards:

![mount_points](mount_points.png)

SSH into your router and run df -h and you should see more space for your router. You should also see the changes in System - Mount points.

![mounts](mounts.png)

Install NFS program so that the router can serve the files from your USB HDD. SSH into your router and run
```
opkg update
opkg install nfs-kernel-server
```

Change the mount to /mnt/ssd and set it to 'rw' instead of 'ro'
```
vi /etc/exports
```

Change the first line to the following (changes need a restart of portmap and nfsd):
```
/mnt/ssd        *(rw,sync,no_subtree_check,insecure,no_root_squash,anonuid=0,anongid=0)
```

Setup IPTables (vi /etc/firewall.user):
```
IPT=iptables
NET_LAN=172.16.0.1/16
IF_LAN=eth0

# portmap
$IPT -I INPUT -j ACCEPT -i $IF_LAN -s $NET_LAN -p tcp --dport 111
$IPT -I INPUT -j ACCEPT -i $IF_LAN -s $NET_LAN -p udp --dport 111

# nfsd
$IPT -I INPUT -j ACCEPT -i $IF_LAN -s $NET_LAN -p tcp --dport 32777:32780
$IPT -I INPUT -j ACCEPT -i $IF_LAN -s $NET_LAN -p udp --dport 32777:32780
```

Start the NFS server:
```
. /etc/init.d/portmap start
. /etc/init.d/portmap enable
. /etc/init.d/nfsd start
. /etc/init.d/nfsd enable
```

Try to mount from Linux:
```
mount -t nfs 172.16.0.1:/mnt/ssd /media/ssd
