## General
The suitcase is used to hold all components of the cluster in one place

## Suitcase setup
The following image shows the setup of the suitcase with a layout plan of all components:

![layout](layout.png)

## Pictures with hardware in the suitcase

Bottom before mounting the parts:
![bottom_preparation](bottom_preparation.JPG)

Bottom after mounting the parts and cables:
![bottom_done](bottom_done.JPG)

Top before mounting the parts:
![top_preparation](top_preparation.JPG)

Top after mounting the parts:
![top_done](top_done.JPG)
