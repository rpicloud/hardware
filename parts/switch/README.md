## General

The switch connects all raspberry pi's with the router. The switch is a TP-Link
TL-SG108E smart switch with web ui.

## Configuration

The switch uses the original firmware from TP-Link
The IP of the switch has been changed to 172.16.0.2 via static dhcp lease in OpenWRT.
For this the switch IP Setting has been changed to DHCP.
