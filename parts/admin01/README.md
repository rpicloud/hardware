## General

This Raspberry Pi 2 is used to show the monitoring interface on the attached display

## Configuration

OS: Raspbian Stretch with desktop
Additional Hardware: 3.5" screen attached via GPIO

## Installed Software

### LCD Driver

Power setting changes can be done with Ansible. For more details see
[Ansible Playbook for the Raspberry Pi cluster](https://gitlab.com/rpicloud/configuration)
in the role 'wireless'

Repository for LCD driver: https://github.com/goodtft/LCD-show

***Manual installation***
- Prepare SD card with Raspbian Stretch
  - sudo dd bs=4M if=2019-04-08-raspbian-stretch.img of=/dev/mmcblk0 conv=fsync
- Disable WiFi and Bluetooth: Open the file /boot/config.txt and add the following lines at the end of the file:
```shell
# disable internal wifi and bluethooth
dtoverlay=pi3-disable-wifi,pi3-disable-bt
```
- Create a file called "ssh" in the /boot directory to enable SSH during boot time
- Unpack the driver LCD-show-master.zip and copy the extracted folder to the SD card (boot directory)
- Start the Raspberry Pi
- cd /boot
- cd LCD-show/
- sudo ./LCD35-show
- Restart

## Initial Setup

```shell
sudo raspi-config
```

- Network options
  - Hostname: admin01
- Boot options
  - Wait for network at boot
- Advanced options
  - Expand file system
  - Memory split: 128


